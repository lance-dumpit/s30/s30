/*
	Mini Activity:
		Create an ExpressJS API designated to port 4000.
		Create a new route with endpoint /hello and method GET.
			-Should be able to respond with "Hello World."
*/

const express = require("express");
const mongoose = require("mongoose");
// mongoose is a pacage that allows creation of Schemas to model our data structure.
// Also has access to a number of methods for manipulating our database

const app = express();

const port = 4000;

mongoose.connect("mongodb+srv://admin:admin123@course-booking.p5sxk.mongodb.net/B157_to-do?retryWrites=true&w=majority", 
	{
		// to avoid error
		useNewUrlParser: true,
		useUnifiedTopology: true

});

// this is not required , this is just to make sure to caught an error
//Set notifications for connection success or failure
// Connection to the database
// Allows us to handle errors when the initial connection is established
let db = mongoose.connection;
// if a connection error occured, output in the console.
// .bind is the error for the whole and for the connection error string, to notify us
db.on("error", console.error.bind(console, "Connnection error"));

// if the connection is successful, output in the console
// open means successful
db.once("open", () => console.log("We're connected to the cloud database"));

// Schemas determine the structure of the documents to be written in the database.
// this acts as a blueprints to our data
// almost same as object constructor
const taskSchema = new mongoose.Schema({
	name: String,//will accept a string data
	status: {
		type: String,
		default: "pending"
	}

});

// ACTIVITY
const userSchema = new mongoose.Schema({
	username: String,//will accept a string data
	password: String
});

// Model uses schemas and are used to create/instantiate objects that correspond to the schema
// naming conventions of the 1st paramater first letter should be Capital letter
// Models must be in a singular form and capitalized.
// the first parameter of the mongoose modek method indicates the collection in where will be stored in the MongoDB collection
// the second parameter is used to specify the schema/blueprint of the documents
// mongooose.model "Task" is the name of the collection, taskSchema will come from the created schema
// Task model /variable will be the name of the model , to perform collections lie adding documents
// Using mongoose, the package was programmed well enough that it automatically converts the similar form of the model name into plural form when creating a collection
const Task = mongoose.model("Task", taskSchema);
// ACTIVITY
const User = mongoose.model("User", userSchema);

// Tasks - parameter will be plural automatically in the database



// middleware , so app will allow us to use json data
// setup for allowing the server to handle data from requests
// allows our app to read json data
app.use(express.json());

//allows our app to read data from forms (json)
// also a middleware
app.use(express.urlencoded({extended:true}))


//Creating a new Task
/*
	1. Add a functionality to check if there are duplicate tasks
		- if the task is already exists in the database, we return an error
		- if the tasks doesn't exist, we add it in our database.
	2. The task data will be coming from the request's body
	3. Create a Task object with a "name" field / property

*/

app.post("/tasks",(req,res) => {
	// Task is the name of the model and the method or request
	// findOne will only find one from the req.body.name
	Task.findOne({name: req.body.name}, (err,result) => {

		if (result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		} else {
			// 
			let newTask = new Task({
				name: req.body.name
			})

			// save method will store the information to the database
			newTask.save((saveErr,savedTask) => {

				// if there are errors in saving
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(201).send("New Task created.")
				}
			})
		}


	})



});

// GET request to retrieve all the documents.

/*
	1. Retrieve all the documents.
	2. If an error is encountered, print the error
	3. If no errors are found, send the success back to the client.

*/

app.get('/tasks', (req,res) => {
	// model.method - same as mongodb's method
	Task.find({}, (err, result) => {

		if (err) {
			console.log(err)
		} else {
			// so response to be in json format
			return res.status(200).json({
				data: result
			})
		}
	})


});

// ACTIVITY

app.post("/signup",(req,res) => {
	
	User.findOne({username: req.body.username}, (err,result) => {

		if (result != null && result.username == req.body.username){
			return res.send("Duplicate user found");
		} else {
			// 
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})

			
			newUser.save((saveErr,savedUser) => {

				
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(201).send("New User created.")
				}
			})
		}


	})



});



app.get('/',(req,res)=>{

	res.send("Hello World");

});




app.listen(port,()=>console.log(`Server is running at port ${port}`));